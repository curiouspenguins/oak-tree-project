# Oak Tree Project

A simple initiative to grow oak trees from seed and plant them. You can read more about the project here: https://otp.curiouspenguins.com/p/about.html

## Roadmap
October 2021 - Seeds collected<br>
November 2021 - Seed stratification starts<br>
Feburary/March 2022 - Sprouted seeds are planted<br>
Spring/Summer 2022 - Seeds are planted in various permanent locations<br>

## Contributing
Please see this page for contributing: https://otp.curiouspenguins.com/p/contribute.html

If you wish to contribute to our site, you can do so with a Google or GitLab account. 

## License
Curious Penguins follow a Copyleft approach with software licensed under a GNU General Public License v3.0

## Project status
This is currently active with trees being planted and cared for. 

## Support
Please reach out to us at Curious Penguins with any questions: https://curiouspenguins.com/#faq
